Pod::Spec.new do |s|
  s.name             = "HOAPlatform"
  s.version          = "1.0.1"
  s.summary          = "HOA Platform for IOS"
  s.description      = <<-DESC
  HOA Platfor mincludes HOAApp and HAOOptions which provide central configuration.
  DESC
  s.homepage         = "https://bitbucket.org/senorshmt/hoaplatformspec"
  
  s.license          = { :type => "MIT", :file => "LICENSE" }
  
  s.author           = "HOUSE OF APPS"
  
  s.platform         = :ios, '8.0'

  s.source       = { :http => "https://www.dropbox.com/s/zrmuc7giapkvxfv/HOAPlatform.zip?dl=1" }

  s.ios.deployment_target = '8.0'
  s.ios.vendored_frameworks = 'HOAPlatform.framework'

  s.ios.dependency 'AFNetworking', '~> 3.0'
  
  s.framework = 'SystemConfiguration', 'UIKit', 'UserNotifications', 'CoreLocation', 'Foundation'
end